const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://root:root@oyclustertest.7pq00ym.mongodb.net/notes_db?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { notesRouter } = require('./routers/notesRouter');

const { authMiddleware } = require('./middleware/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

app.use(errorHandler);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res, next) {
  if (err.stack) {
    return res.status(400).send({ message: err.message });
  }
  return res.status(500).send({ message: err.message });
}

const mongoose = require('mongoose');
const Joi = require('joi');

const Note = mongoose.model('Note', {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
});

const createNoteJoiSchema = Joi.object({
  text: Joi.string()
    .required(),
});

const getNotesJoiSchema = Joi.object({
  offset: Joi.number()
    .integer(),
  limit: Joi.number()
    .integer(),
});

const idParamJoiSchema = Joi.object({
  id: Joi.string()
    .hex().length(24)
    .required(),
});

module.exports = {
  Note,
  createNoteJoiSchema,
  getNotesJoiSchema,
  idParamJoiSchema,
};

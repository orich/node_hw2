const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model('User', {
  name: {
    type: String,
    required: false,
  },
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
});

const userJoiSchema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(2)
    .max(30)
    .required(),

  password: Joi.string()
    .pattern(/[a-zA-Z0-9]{3,30}$/),

  name: Joi.string()
    .alphanum()
    .min(2)
    .max(60),
});

const changePasswordJoiSchema = Joi.object({
  oldPassword: Joi.string()
    .pattern(/[a-zA-Z0-9]{3,30}$/)
    .required(),

  newPassword: Joi.string()
    .pattern(/[a-zA-Z0-9]{3,30}$/)
    .required(),
});

const userLoginJoiShema = Joi.object({
  username: Joi.string()
    .required(),

  password: Joi.string()
    .required(),
});

module.exports = {
  User,
  userJoiSchema,
  userLoginJoiShema,
  changePasswordJoiSchema,
};

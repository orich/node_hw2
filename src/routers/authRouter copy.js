const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const router = express.Router();
const { registerUser, loginUser } = require('../controllers/authController');

// router.post('/register', (req, res, next) => res.json({ message: 'hello' }));
// router.post('/login', (req, res, next) => res.json({ message: 'hello' }));

router.post('/register', asyncWrapper(registerUser));
router.post('/login', asyncWrapper(loginUser));

module.exports = {
  authRouter: router,
};

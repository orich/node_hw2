const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const router = express.Router();
const { getUserProfile, deleteUser, changeUserPassword } = require('../controllers/usersController');

router.get('/me', asyncWrapper(getUserProfile));
router.patch('/me', asyncWrapper(changeUserPassword));
router.delete('/me', asyncWrapper(deleteUser));

module.exports = {
  usersRouter: router,
};

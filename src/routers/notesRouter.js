const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const router = express.Router();
const {
  createNote, getNotes, getNote, updateNote, checkNote, deleteNote,
} = require('../controllers/notesController');

router.post('/', asyncWrapper(createNote));
router.get('/', asyncWrapper(getNotes));
router.get('/:id', asyncWrapper(getNote));
router.put('/:id', asyncWrapper(updateNote));
router.patch('/:id', asyncWrapper(checkNote));
router.delete('/:id', asyncWrapper(deleteNote));

module.exports = {
  notesRouter: router,
};

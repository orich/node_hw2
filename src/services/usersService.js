const bcryptjs = require('bcryptjs');
const { User } = require('../models/users');

class UsersService {
  static async getUser(name) {
    return User.findOne({ username: name });
  }

  static async getUserById(userId) {
    return User.findById(userId);
  }

  static async saveUser({ name, username, password }) {
    const user = new User({
      name,
      username,
      password: await bcryptjs.hash(password, 10),
    });

    return user.save();
  }

  static async getUserProfile(userId) {
    const { _id, username, createdDate } = await User.findOne({ _id: userId });
    return { user: { _id, username, createdDate } };
  }

  static async deleteUser(userId) {
    return User.findByIdAndDelete(userId);
  }

  static async updatePassword(userId, password) {
    return User
      .findByIdAndUpdate(userId, { $set: { password: await bcryptjs.hash(password, 10) } });
  }
}

module.exports = {
  UsersService,
};

const { Note } = require('../models/notes');

class NotesService {
  static async saveNote(text, userId) {
    const note = new Note({
      text,
      userId,
    });

    return note.save();
  }

  static async getNotes(userId, offset, limit) {
    const notes = await Note.find({ userId })
      .skip(offset)
      .limit(limit);
    const count = await Note.count({ userId });
    return { notes, count };
  }

  static async getNote(noteId, userId) {
    let note = await Note.findOne({ _id: noteId, userId });
    if (!note) {
      return null;
    }
    note = note.toObject();
    delete note.__v;
    return note;
  }

  static async updateNote(noteId, userId, text) {
    const note = await Note.findOne({ _id: noteId, userId });
    if (!note) return null;
    if (text) note.text = text;

    return note.save();
  }

  static async checkNote(noteId, userId) {
    const note = await Note.findOne({ _id: noteId, userId });
    if (!note) return null;
    return Note
      .findByIdAndUpdate({ _id: noteId, userId }, { $set: { completed: !note.completed } });
  }

  static async deleteNote(noteId, userId) {
    return Note.findByIdAndDelete({ _id: noteId, userId });
  }
}

module.exports = {
  notesService: NotesService,
};

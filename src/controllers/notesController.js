const { notesService } = require('../services/notesService');
const { createNoteJoiSchema, getNotesJoiSchema, idParamJoiSchema } = require('../models/notes');

async function createNote(req, res, next) {
  const { text } = req.body;
  await createNoteJoiSchema.validateAsync({ text });

  notesService.saveNote(req.body.text, req.user.userId).then(
    res.json({ message: 'Your note was successfully created' }),
  );
}

async function getNotes(req, res, next) {
  const { offset, limit } = req.query;
  await getNotesJoiSchema.validateAsync({ offset, limit });

  const result = await notesService.getNotes(req.user.userId, +offset, +limit);
  return res.json({
    offset,
    limit,
    count: result.count,
    notes: result.notes,
  });
}

async function getNote(req, res, next) {
  const { id } = req.params;
  await idParamJoiSchema.validateAsync({ id });

  const note = await notesService.getNote(id, req.user.userId);
  if (!note) {
    return res.status(404).json({ message: 'Note not found' });
  }
  return res.json({ note });
}

async function updateNote(req, res, next) {
  const { id } = req.params;
  const { text } = req.body;
  await idParamJoiSchema.validateAsync({ id });

  const note = await notesService.updateNote(id, req.user.userId, text);
  if (!note) {
    return res.status(404).json({ message: 'Update failed' });
  }
  return res.status(200).json({ message: 'Note was successfully updated' });
}

async function checkNote(req, res, next) {
  const { id } = req.params;
  await idParamJoiSchema.validateAsync({ id });

  const note = await notesService.checkNote(id, req.user.userId);
  if (!note) {
    return res.status(404).json({ message: 'Check note failed' });
  }
  return res.status(200).json({ message: 'Note was successfully checked' });
}

async function deleteNote(req, res, next) {
  const { id } = req.params;
  await idParamJoiSchema.validateAsync({ id });

  const note = await notesService.deleteNote(id, req.user.userId);
  if (!note) {
    return res.status(404).json({ message: 'Delete note failed' });
  }
  return res.status(200).json({ message: 'Note was successfully deleted' });
}

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};

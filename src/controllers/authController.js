const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const { userJoiSchema, userLoginJoiShema } = require('../models/users');
const { UsersService } = require('../services/usersService');

const registerUser = async (req, res, next) => {
  const { name, username, password } = req.body;
  await userJoiSchema.validateAsync({ name, username, password });

  await UsersService.saveUser({ name, username, password });
  return res.status(200).json({ message: 'The user was successfully registered' });
};

const loginUser = async (req, res) => {
  const { username, password } = req.body;
  await userLoginJoiShema.validateAsync({ username, password });

  const user = await UsersService.getUser(username);
  if (user && await bcryptjs.compare(String(password), String(user.password))) {
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};

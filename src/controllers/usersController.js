const bcryptjs = require('bcryptjs');

const { UsersService } = require('../services/usersService');
const { changePasswordJoiSchema } = require('../models/users');

const getUserProfile = async (req, res, next) => {
  const user = await UsersService.getUserProfile(req.user.userId);
  return res.json(user);
};

const deleteUser = async (req, res, next) => {
  const user = await UsersService.deleteUser(req.user.userId);
  if (!user) {
    res.status(403).json({ message: 'Not deleted' });
  }
  return res.json({ message: 'Success' });
};

const changeUserPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  await changePasswordJoiSchema.validateAsync({ oldPassword, newPassword });

  const user = await UsersService.getUserById(req.user.userId);
  if (!user) {
    return res.status(403).json({ message: 'Not found' });
  }
  if (await bcryptjs.compare(String(oldPassword), String(user.password))) {
    const resault = await UsersService.updatePassword(req.user.userId, newPassword);
    if (!resault) {
      return res.status(403).json({ message: 'Not changed' });
    }
  } else {
    return res.status(403).json({ message: 'Old passport is wrong' });
  }

  return res.json({ message: 'Password was successfully changed' });
};

module.exports = {
  getUserProfile,
  deleteUser,
  changeUserPassword,
};
